let FAClient = null;

let token, endpoint;

function startupService() {

    console.log("Applet Excecuted");
  
    FAClient = new FAAppletClient({
      appletId: SERVICE.appletId,
    });

    token = FAClient.params.apiToken;
    endpoint = FAClient.params.endpoint;
}

const showMessage = (type,message,accountName) => {
  document.getElementById(PAGE_OBJECT.loading).hidden = true;

  if(!type){
    return;
  }

  if(accountName){
    let accountDiv = document.getElementById(PAGE_OBJECT.accountDiv);
    accountDiv.innerHTML = `<p>${accountName}</p>`;
    accountDiv.hidden = false
  }

  let messageDiv = document.getElementById(PAGE_OBJECT.messageDiv);
  messageDiv.innerHTML = "";

  let textMessage = ALERT_TEMPLATE.replace(/{replace}/g,type.tag);
  
  messageDiv.innerHTML = textMessage;

  document.getElementById(PAGE_OBJECT.alertMessage).innerHTML = message;
  messageDiv.hidden = false;
}

const searchAccountFunction = async () => {

  let alertMessage = document.getElementById(PAGE_OBJECT.alertMessage);
  let accountDiv = document.getElementById(PAGE_OBJECT.accountDiv);

  if(alertMessage && !alertMessage.hidden){
    alertMessage.hidden = true;
  }

  if(accountDiv && !accountDiv.hidden){
    accountDiv.hidden = true;
  }

  document.getElementById(PAGE_OBJECT.loading).hidden = false;
  let nmlsAccount = document.getElementById(PAGE_OBJECT.searchInput).value;

  try{

    let response = await executeQuery(token, endpoint, {
      'nmls' : nmlsAccount 
    });
    
    if(response.data.Message != "NMLS does not Exist."){
      let responseMessage = `${response.data.Message}`;
      showMessage(MESSAGES.success,`<b>Account Name :</b> ${response.data.AccountName}`);
    }else{
      showMessage(MESSAGES.warning,response.data.Message);
    }

  }catch(e){
    showMessage(MESSAGES.error,"Internal Error, please contact support");
    console.log(e);
  }
}

const hidden = (mainContainer,waitingText) => {
  document.getElementById(PAGE_OBJECT.mainContainer).hidden = mainContainer;
  document.getElementById(PAGE_OBJECT.waitingText).hidden = waitingText;
}

  