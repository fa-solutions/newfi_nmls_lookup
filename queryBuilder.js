const executeQuery = async (token,url,data) => {

    let config = {
        headers : { 
            "Authorization": token, 
            "Content-Type": "application/json"
        },
        method : "POST",
        url : url,
        data : data
    }

    return axios(config);
};
