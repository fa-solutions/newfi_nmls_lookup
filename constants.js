const SERVICE = {
    name: 'NMLS Lookup',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL25ld2ZpX25tbHNfbG9va3Vw`
};

const FREEAGENT_WEBHOOK = 'https://freeagent.network/webhook/251919d3-493e-4f41-a1f7-49b6f264b642';

const PAGE_OBJECT = {
    waitingText : "loading-text",
    mainContainer : "MainContainer",
    searchInput : "SearchInput",
    bodyTable : "searchTable",
    footerContainer : "footer",
    alertMessage : "Message",
    messageDiv : "MessageDiv",
    loading : "loading",
    radio : "select",
    sequenceDiv : "sequenceId",
    accountDiv : "accountDiv"
};
  
const ALERT_TEMPLATE = "<div {replace} id='Message'></div>";

const MESSAGES = {
    error : {
      type : "ERROR",
      tag : `class="alert alert-danger" role="alert"`
    },
    success : {
      type :"SUCCESS",
      tag : `class="alert alert-success" role="alert"`
    },
    warning : {
      type : "WARNING",
      tag : `class="alert alert-warning" role="alert"`
    }
};
  
